HadoopControl
=============

Ferramenta de Gerenciamento e Execução de Experimentos no Cluster Hadoop  do
Departamento de Informática da UFPR. Para qualquer dúvida ou ajuda envie um
email para:

> erlfilho em inf ponto ufpr ponto br.

Instalação prática
==================

> $ git clone https://github.com/erlfilho/hadoopctl.git

> $ cd hadoopctl/

> $ vim conf/hadoopctl.conf

> export HADOOPCTL_USER="seu_login"

> $ source conf/hadoopctl.conf

> $ hadoopctl --setup

> $ hadoopctl -t "sua_instalacao_hadoop_ja_configurada"

> $ hadoopctl --help

> $ jobctl -a # Inicia o SysStat para monitorar CPU, MEM e Disco.

> $ jobctl -k all

> $ jobctl -h

Instalação detalhada
====================

